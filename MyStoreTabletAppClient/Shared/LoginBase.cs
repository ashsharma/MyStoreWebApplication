﻿using Microsoft.AspNetCore.Components;
using MyStoreTabletAppClient.Data.Models;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace MyStoreTabletAppClient.Shared
{
    public class LoginBase : LayoutComponentBase
    {
        public string UserName = "";
        public string Password = "";
        string baseUrl = "https://localhost:5001/";
        HttpClient http = new HttpClient();
        public Credential[] credential;
        protected override async Task OnInitializedAsync()
        {
            try
            {
                credential = await http.GetJsonAsync<Credential[]>(baseUrl + "Credential.json");
            }
            catch (Exception ex)
            {

            }
        }
    }
}
