﻿using MyStoreTabletAppClient.Data.Models;
using Microsoft.AspNetCore.Components;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Authentication;
using System.Threading.Tasks;

namespace MyStoreTabletAppClient.Pages
{
    public class AddEditStoreBase : ComponentBase
    {
        string baseUrl = "http://10.6.181.69/cfmapi/api/";
        HttpClient Http = new HttpClient();
        protected int selectedRegion;
        protected List<DistributionRegion> regions;
        protected StoreOut storeObj=new StoreOut();


        protected override async Task OnInitializedAsync()
        {
            try
            {
                Http.DefaultRequestHeaders.Add("CountryId", "1");
                await GetRegionData(Http);             
            }
            catch (Exception ex)
            {

            }
        }

        // Add New Customer Details Method
        protected async Task saveStoreItem()
        {
            try
            {
                if (storeObj != null)
                {

                    await Http.SendJsonAsync(HttpMethod.Post, baseUrl + "store", storeObj);
                    //var updatedStores = await Http.GetJsonAsync<StoreOut[]>(baseUrl + "store");
                }
                //else
                //{
                //    await Http.SendJsonAsync(HttpMethod.Put, baseUrl + "api/CustomerMasters/" + custObj.CustCd, custObj);
                //    custs = await Http.GetJsonAsync<CustomerMaster[]>(baseUrl + "api/CustomerMasters/");
                //}

                //showAddrow = false;
            }
            catch (Exception ex)
            {

            }
        }

        private async Task GetRegionData(HttpClient Http)
        {
            var regionData = await Http.GetJsonAsync<ApiOut>(baseUrl + "Reference/GetCategoryValues?categoryName=distribution_region");
            regions = JsonConvert.DeserializeObject<List<DistributionRegion>>(Convert.ToString(regionData.Data));
        }
    }
}
