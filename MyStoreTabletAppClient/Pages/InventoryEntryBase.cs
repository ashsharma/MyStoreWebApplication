﻿using Microsoft.AspNetCore.Components;
using MyStoreTabletAppClient.Data;
using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using Mcd.MyStore.InventoryService.Common.Definitions;
using MyStoreTabletAppClient.Data.CountingListEngine;
using MyStoreTabletAppClient.Data.Models;
using Radzen;
using Radzen.Blazor;

namespace MyStoreTabletAppClient.Pages
{
    public class InventoryEntryBase : ComponentBase
    {
        HttpClient httpClient = new HttpClient();
        protected Warehouse[] warehouse;

        protected Warehouse warehouseObj;

        string ids = "0";
        protected bool showAddrow = false;

        protected bool loadFailed;
        string baseUrl = "https://localhost:5001/";

        public int count = 0;
        public bool isEditable = false;
        public string isEditDisplay = "block";
        public string isUpdateDisplay = "none";
        public IList<WarehouseItemDto> warehouseList;
        public IList<CountingList> countingList;
        public IList<WarehouseItemDto> selectedItemWarehouses;
        public IList<CountingList> countingListForAllWarehouse;
        public SelectedCountingData selectedCountingData;
        public decimal? updatedCaseUnitCountTotal;
        public decimal? updatedSlaveUnitCountTotal;
        public decimal? updatedSingleUnitCountTotal;
        public decimal? updatedTotalUnitCountTotal;
        public string activeTabClass = "active";
        public string defaultActiveTabClass = "active";
        public int activeTabWId = -1;
        public CountingList selectedCountingList;
        public void makeEditable()
        {
            isEditable = true;
            isEditDisplay = "none";
            isUpdateDisplay = "block";
        }
        public void cancelEditable()
        {
            isEditable = false;
            isEditDisplay = "block";
            isUpdateDisplay = "none";
        }
        public void updateInventory()
        {
            isEditable = false;
            isEditDisplay = "block";
            isUpdateDisplay = "none";

            string warehouseOut = Newtonsoft.Json.JsonConvert.SerializeObject(warehouse, Newtonsoft.Json.Formatting.Indented);
            File.WriteAllText("wwwroot/WarehouseItems.json", warehouseOut);
        }

        protected override async Task OnInitializedAsync()
        {
            try
            {
                await GetWarehouseCountingData();
            }
            catch (Exception ex)
            {

            }
        }

        private async Task GetWarehouseCountingData()
        {
            //CountingListModal regionData = await httpClient.GetJsonAsync<CountingListModal>(baseUrl + "GetCountingList.json");
            //this.warehouseList = regionData.Data.WarehouseList;

            //var countingData = regionData.Data.CountingList;
            ////  JArray countingDataArray = new JArray(countingData.First().Children().ToList());

            //this.countingListForAllWarehouse = countingData;
            //GetSelectedWarehoouseData(this.countingListForAllWarehouse.FirstOrDefault());
            //GetCountingWarehouseWise(-1);


            var regionData = await httpClient.GetJsonAsync<object>("http://localhost:8889/inventory/GetCountingList/json?CountingType=Daily&BusinessDay=2019-09-13");
            var regions = JsonConvert.DeserializeObject<object>(Convert.ToString(regionData));
            var data = ((Newtonsoft.Json.Linq.JObject)regions).First;
            var obj1 = data.Root.ToList().Values().Children().Values().ToList();
            var warehouseList = obj1[2].Children().ToList()[1];

            var dataa = (warehouseList.First().Children().ToList());

            JArray arr = new JArray(dataa);

            this.warehouseList = arr.ToObject<IList<WarehouseItemDto>>();

            var countingData = obj1[2].Children().ToList()[2];
            JArray countingDataArray = new JArray(countingData.First().Children().ToList());

            this.countingListForAllWarehouse = countingDataArray.ToObject<IList<CountingList>>();
            GetSelectedWarehoouseData(this.countingListForAllWarehouse.FirstOrDefault());
            GetCountingWarehouseWise(-1);
        }

        public void GetCountingWarehouseWise(int wId)
        {
            activeTabWId = wId;
            defaultActiveTabClass = (wId == -1 ? "active" : "");
            this.countingList = (from a in this.countingListForAllWarehouse
                                 select new CountingList()
                                 {
                                     PosRefID = a.PosRefID,
                                     Name = a.Name,
                                     MeasureCode = a.MeasureCode,
                                     UnitsPerCase = a.UnitsPerCase,
                                     SlaveUnitsPerCase = a.SlaveUnitsPerCase,
                                     PricePerCase = a.PricePerCase,
                                     Counting = a.Counting.Where(x => x.WarehouseID == (wId == -1 ? x.WarehouseID : wId)).ToList()
                                 }).Where(a => a.Counting.Count() > 0).ToList();
            selectedCountingList = this.countingList.ToList().Find(x => x.PosRefID == selectedCountingData.PosRefID);

        }

        public void GetSelectedWarehoouseData(CountingList data)
        {
            selectedItemWarehouses = data.Counting.Select(wHouse => new WarehouseItemDto()
            {
                ID = wHouse.WarehouseID,
                Name = warehouseList.FirstOrDefault(a => a.ID == wHouse.WarehouseID).Name
            }).ToList();
            selectedCountingData = new SelectedCountingData();
            selectedCountingData.PosRefID = data.PosRefID;
            selectedCountingData.Name = data.Name;
            selectedCountingData.MeasureCode = data.MeasureCode;
            selectedCountingData.UnitsPerCase = data.UnitsPerCase;
            selectedCountingData.SlaveUnitsPerCase = data.SlaveUnitsPerCase;
            selectedCountingData.PricePerCase = data.PricePerCase;
            selectedCountingData.CaseUnitCount = data.Counting.FirstOrDefault().CaseUnitCount;
            selectedCountingData.SlaveUnitCount = data.Counting.FirstOrDefault().SlaveUnitCount;
            selectedCountingData.UnitCount = data.Counting.FirstOrDefault().UnitCount;
            selectedCountingData.TotalUnitCount = data.Counting.FirstOrDefault().TotalUnitCount;
            selectedCountingData.WarehouseID = data.Counting.FirstOrDefault().WarehouseID;
            selectedCountingData.ExpectedUnitCount = data.Counting.FirstOrDefault().ExpectedUnitCount;

            updatedCaseUnitCountTotal = Convert.ToDecimal(selectedCountingData.CaseUnitCount) * Convert.ToDecimal(selectedCountingData.UnitsPerCase);
            updatedSlaveUnitCountTotal = Convert.ToDecimal(selectedCountingData.SlaveUnitCount) * Convert.ToDecimal(selectedCountingData.SlaveUnitsPerCase);
            updatedSingleUnitCountTotal = Convert.ToDecimal(selectedCountingData.UnitCount);
            updatedTotalUnitCountTotal = updatedCaseUnitCountTotal + updatedSlaveUnitCountTotal + updatedSingleUnitCountTotal;
        }

        public void updateTotalUnits(int itemId)
        {
            var updatedRow = warehouse.FirstOrDefault(x => x.ItemId == itemId);
            updatedRow.Total = (updatedRow.CaseUnit * updatedRow.Unit) + (updatedRow.SubUnit * updatedRow.Unit) + updatedRow.Unit;
            updatedRow.Expected = updatedRow.CaseUnit + updatedRow.Unit + updatedRow.SubUnit;
            updatedRow.Counted = true;
        }

        public void UpdateCaseUnitCalc(object e, int posRefID, int warehouseID, decimal caseUnitsPerCase)
        {
            decimal changedCaseUnitCount = Convert.ToDecimal(e);
            var selectedData = this.countingListForAllWarehouse.FirstOrDefault(x => x.PosRefID == posRefID);
            var selectedWarehouseData = selectedData.Counting.FirstOrDefault(x => x.WarehouseID == warehouseID);
            selectedWarehouseData.CaseUnitCount = changedCaseUnitCount;
            updatedCaseUnitCountTotal = changedCaseUnitCount * caseUnitsPerCase;
            selectedWarehouseData.TotalUnitCount = updatedCaseUnitCountTotal + Convert.ToDecimal(updatedSlaveUnitCountTotal) + Convert.ToDecimal(updatedSingleUnitCountTotal);
            updatedTotalUnitCountTotal = selectedWarehouseData.TotalUnitCount;
        }

        public void UpdateSlaveUnitCalc(object e, int posRefID, int warehouseID, decimal slaveUnitsPerCase)
        {
            decimal changedSlaveUnitCount = Convert.ToDecimal(e);
            var selectedData = this.countingListForAllWarehouse.FirstOrDefault(x => x.PosRefID == posRefID);
            var selectedWarehouseData = selectedData.Counting.FirstOrDefault(x => x.WarehouseID == warehouseID);
            selectedWarehouseData.SlaveUnitCount = changedSlaveUnitCount;
            updatedSlaveUnitCountTotal = changedSlaveUnitCount * slaveUnitsPerCase;
            selectedWarehouseData.TotalUnitCount = updatedCaseUnitCountTotal + Convert.ToDecimal(updatedSlaveUnitCountTotal) + Convert.ToDecimal(updatedSingleUnitCountTotal);
            updatedTotalUnitCountTotal = selectedWarehouseData.TotalUnitCount;
        }

        public void UpdateSingleUnitCalc(object e, int posRefID, int warehouseID)
        {
            decimal changedCaseUnitCount = Convert.ToDecimal(e);
            var selectedData = this.countingListForAllWarehouse.FirstOrDefault(x => x.PosRefID == posRefID);
            var selectedWarehouseData = selectedData.Counting.FirstOrDefault(x => x.WarehouseID == warehouseID);
            selectedWarehouseData.UnitCount = changedCaseUnitCount;
            updatedSingleUnitCountTotal = changedCaseUnitCount;
            selectedWarehouseData.TotalUnitCount = updatedCaseUnitCountTotal + Convert.ToDecimal(updatedSlaveUnitCountTotal) + Convert.ToDecimal(updatedSingleUnitCountTotal);
            updatedTotalUnitCountTotal = selectedWarehouseData.TotalUnitCount;
        }

        public void OpenRespectiveWarehouseTab(int wId)
        {
            this.GetCountingWarehouseWise(wId);
            activeTabWId = wId;
            defaultActiveTabClass = (wId == -1 ? "active" : "");
        }

        public class Result
        {
            public bool Successful { get; set; }
            public string ErrorCode { get; set; }
            public object Data { get; set; }
        }

        public class WarehouseItemDto
        {
            public int ID { get; set; }
            public string Name { get; set; }
        }

        public class CountingList
        {
            public int PosRefID { get; set; }
            public string Name { get; set; }
            public string MeasureCode { get; set; }
            public decimal UnitsPerCase { get; set; }
            public decimal SlaveUnitsPerCase { get; set; }
            public decimal PricePerCase { get; set; }
            public List<Counting> Counting { get; set; }
        }

        public class Counting
        {
            public int WarehouseID { get; set; }
            public int? StoragePosition { get; set; }
            public decimal? UnitCount { get; set; }
            public decimal? SlaveUnitCount { get; set; }
            public decimal? CaseUnitCount { get; set; }
            public decimal? TotalUnitCount { get; set; }
            public decimal? ExpectedUnitCount { get; set; }
            public bool IsCounted { get; set; }
        }

        public class SelectedCountingData
        {
            public int PosRefID { get; set; }
            public string Name { get; set; }
            public string MeasureCode { get; set; }
            public decimal UnitsPerCase { get; set; }
            public decimal SlaveUnitsPerCase { get; set; }
            public decimal PricePerCase { get; set; }
            public int WarehouseID { get; set; }
            public string StoragePosition { get; set; }
            public decimal? UnitCount { get; set; }
            public decimal? SlaveUnitCount { get; set; }
            public decimal? CaseUnitCount { get; set; }
            public decimal? TotalUnitCount { get; set; }
            public decimal? ExpectedUnitCount { get; set; }
            public bool IsCounted { get; set; }
        }

        public class CountingListPostDTO
        {
            public string BusinessDay { get; set; }
            public CountingListType CountingType { get; set; }
            public List<CountingList> CountingList { get; set; }
        }

        public class CountingListContainerDto
        {
            #region Constructor

            public CountingListContainerDto()
            {
                Warehouses = new List<WarehouseItemDto>();
                Items = new List<CountingListItemDto>();
                CountingLists = new List<CountingList>();
            }

            #endregion

            public CountingListType CountingListType { get; set; }

            public List<WarehouseItemDto> Warehouses { get; set; }
            public List<CountingListItemDto> Items { get; set; }
            public string Name { get; set; }

            public bool IsCompleted;
            public string Witness { get; set; }

            public List<CountingList> CountingLists { get; set; }

            public DateTime? Timestamp { get; set; }
        }

        protected async Task UpdateCountingData(int PosRefID, int warehouseID)
        {
            try
            {
                var updatedCountedData = this.countingListForAllWarehouse.FirstOrDefault(x => x.PosRefID == PosRefID);
                updatedCountedData.Counting.FirstOrDefault(x => x.WarehouseID == warehouseID).IsCounted = true;

                CountingListPostDTO data = new CountingListPostDTO();
                data.CountingList = new List<CountingList>();
                data.BusinessDay = "2019-09-13";
                data.CountingType = CountingListType.Full;
                data.CountingList.Add(updatedCountedData);
                data.CountingList.ToList().ForEach(a =>
                {
                    a.Counting.ForEach(b =>
                    {
                        b.UnitCount = Convert.ToDecimal(b.UnitCount);
                        b.SlaveUnitCount = Convert.ToDecimal(b.SlaveUnitCount);
                        b.CaseUnitCount = Convert.ToDecimal(b.CaseUnitCount);
                    });
                });
                var url = "http://localhost:8889/inventory/SaveInventoryCount/json";
                await httpClient.SendJsonAsync(HttpMethod.Post, url, data);
                await GetWarehouseCountingData();
            }
            catch (Exception ex)
            {

            }
        }

        protected void RowRender(RowRenderEventArgs<CountingList> args)
        {

            //args.Attributes.Add("style", $"font-weight: {(args.Data.Quantity > 20 ? "bold" : "normal")};");
        }

        protected void LoadData()
        {

            //args.Attributes.Add("style", $"font-weight: {(args.Data.Quantity > 20 ? "bold" : "normal")};");
        }
    }
}