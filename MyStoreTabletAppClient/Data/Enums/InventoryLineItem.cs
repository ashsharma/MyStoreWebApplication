﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mcd.MyStore.InventoryService.Common.Definitions
{
    public class InventoryLineItem
    {
        public int ItemID { get; set; }
        public DateTime? EndDateTimeStamp { get; set; }
        public int TransactionID { get; set; }
        public decimal? UnitAmount { get; set; }
        public decimal SlaveUnitAmount { get; set; }
        public decimal CaseAmount { get; set; }
        public int? WarehouseID { get; set; }
        public decimal ExpectedUnitCount { get; set; }
        public decimal PricePerCase { get; set; }
        public decimal UnitsPerCase { get; set; }
    }
}
