﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mcd.MyStore.InventoryService.Common.Definitions
{
    public enum FoodCostIdentifier
    {
        MTD = 0, // MonthToDate
        MTH = 1, // Month
        WEK = 2  // Week
    }
    /// <summary>
    /// Keys which are used to identify the data in the 'OperationsReportFoodPaperCostsResponse'
    /// The resulting language keys are "KEY_OperationsReport_FoodPaperCosts_" + the given report key
    /// </summary>
    public static class OPSReportKey
    {
        public static readonly string OpeningStock = "OpeningStock";
        public static readonly string Deliveries = "Deliveries";
        public static readonly string Transfers = "Transfers";
        public static readonly string ClosingStock = "ClosingStock";
        public static readonly string ActualUsageCost = "ActualUsageCost";
        public static readonly string BaseCostFromProductMix = "BaseCostFromProductMix";
        public static readonly string RawWaste = "RawWaste";
        public static readonly string FullWaste = "FullWaste";
        public static readonly string TotalWaste = "TotalWaste";
        public static readonly string RawPromo = "RawPromo";
        public static readonly string FullPromo = "FullPromo";
        public static readonly string TotalPromo = "TotalPromo";
        public static readonly string CrewMeals = "CrewMeals";
        public static readonly string ManagerMeals = "ManagerMeals";
        public static readonly string TotalMeals = "TotalMeals";
        public static readonly string CustomerDiscount = "CustomerDiscount";
        public static readonly string NonRecipeCosts = "NonRecipeCosts";
        public static readonly string TotalTargetFoodCost = "TotalTargetFoodCost";
        public static readonly string POSUsageWithoutRawPromoAndRawWaste = "POSUsageWithoutRawPromoAndRawWaste";
        public static readonly string DeviationInNative = "DeviationInNative";
        public static readonly string DeviationAgainstPOSUsage = "DeviationAgainstPOSUsage";
        public static readonly string RegionalTargetFoodCostPercent = "RegionalTargetFoodCostPercent";
        public static readonly string DeviationBetweenActualAndRegional = "DeviationBetweenActualAndRegional";
        public static readonly string YieldCalculation = "YieldCalculation";        
    }
}
