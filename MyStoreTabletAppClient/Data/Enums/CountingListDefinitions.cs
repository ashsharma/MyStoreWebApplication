﻿namespace Mcd.MyStore.InventoryService.Common.Definitions
{
    public enum CountingListType
    {
        Daily,
        Full,
        DailyLegacy,
        WeeklyLegacy,
        MonthlyLegacy
    }
}
