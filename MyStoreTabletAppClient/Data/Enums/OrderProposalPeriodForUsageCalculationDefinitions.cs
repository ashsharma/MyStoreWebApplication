﻿
namespace Mcd.MyStore.InventoryService.Common.Definitions
{
    public enum ProposalPeriod
    {
        LastDay,
        LastWeek,
        LastMonth,
        UserDefined
    }
}
