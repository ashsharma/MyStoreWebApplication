﻿
namespace Mcd.MyStore.InventoryService.Common.Definitions
{
    public enum PurchaseTransactionStatus
    {
        Draft,
        Open,
        PartiallyDelivered,
        FullyDelivered
    }
}
