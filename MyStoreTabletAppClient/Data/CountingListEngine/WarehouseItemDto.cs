﻿using System;

namespace MyStoreTabletAppClient.Data.CountingListEngine
{
    public class WarehouseItemDto
    {
        public int ID { get; set; }
        public string Description { get; set; }

        #region EQUALS OVERRIDE

        public override bool Equals(Object obj)
        {
            if (obj == null)
            {
                return false;
            }

            WarehouseItemDto p = obj as WarehouseItemDto;
            if ((Object)p == null)
            {
                return false;
            }

            return (ID == p.ID);
        }

        public bool Equals(WarehouseItemDto p)
        {
            if ((object)p == null)
            {
                return false;
            }

            return (ID == p.ID);
        }

        public override int GetHashCode()
        {
            return ID;
        }

        #endregion
    }
}
