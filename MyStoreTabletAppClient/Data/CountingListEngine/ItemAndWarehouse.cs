﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyStoreTabletAppClient.Data.CountingListEngine
{
    public class ItemAndWarehouse
    {
        public int AssemblyItemItemID { get; set; }
        public int ShippingUnitItemID { get; set; }
        public int WarehouseID { get; set; }
        public string ItemType { get; set; }
        public int McdPOSRefID { get; set; }
        public int? InventoryAccountingMethodCode { get; set; }
        public int? RemapToShippingUnit { get; set; }

        public int SupplierItemID { get; set; }

        public string Comment { get; set; }

        public bool IsDelistedItem { get; set; }

        public bool IsTemporaryLocked { get; set; }

        public DateTime? InventoryExpirationDate { get; set; }

        public decimal? UnitsPerCase { get; set; }

        public decimal? SlaveUnitsPerCase { get; set; }

        public decimal PricePerCase { get; set; }

        public string CaseUPCEANNumber { get; set; }

        public int? StoragePlaceID { get; set; }

        public string CaseUnitOfMeasureCode { get; set; }

        public string Description { get; set; }

        public string UnitOfMeasureCode { get; set; }

        public string SlaveUnitsMeasureCode { get; set; }

        public string McdProductCode { get; set; }
        public string EnvironmentalCode { get; set; }

        public string UnitsOfMeasureDescription { get; set; }

        public bool AutomaticCountingListAssignmentPerformed { get; set; }

        public string WarehouseDescription { get; set; }
    }
}
