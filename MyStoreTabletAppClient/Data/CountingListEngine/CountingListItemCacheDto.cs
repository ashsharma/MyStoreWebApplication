﻿using Mcd.MyStore.InventoryService.Common.Definitions;

namespace MyStoreTabletAppClient.Data.CountingListEngine
{
    public class CountingListItemCacheDto : CountingListItemDto
    {
        //public CountingListType CountingListType { get; set; }

        public static CountingListItemCacheDto FromCountingListItemDto(CountingListItemDto viewModel)
        {
            return new CountingListItemCacheDto
            {
                Timestamp = viewModel.Timestamp,
                ItemID = viewModel.ItemID,
                UnitsPerCase = viewModel.UnitsPerCase,
                SlaveUnitsPerCase = viewModel.SlaveUnitsPerCase,
                SlaveUnitOfMeasureCode = viewModel.SlaveUnitOfMeasureCode,
                CaseUnitOfMeasureCode = viewModel.CaseUnitOfMeasureCode,
                UnitOfMeasureDescription = viewModel.UnitOfMeasureDescription,
                UnitOfMeasureCode = viewModel.UnitOfMeasureCode,
                PricePerCase = viewModel.PricePerCase,
                McDProductCode = viewModel.McDProductCode,
                EnvironmentalCode = viewModel.EnvironmentalCode,
                McDPOSRefID = viewModel.McDPOSRefID,
                Description = viewModel.Description,
                LocationID = viewModel.LocationID,
                StoragePlaceID = viewModel.StoragePlaceID,
                LocationShortName = viewModel.LocationShortName,
                IsCounted = viewModel.IsCounted,
                IsItemAlreadyCountedForBusinessDay = viewModel.IsItemAlreadyCountedForBusinessDay,
                ExpectedUnitCount = viewModel.ExpectedUnitCount,
                TargetStockWithoutInventoryCounts = viewModel.TargetStockWithoutInventoryCounts,
                UnitCount = viewModel.UnitCount,
                CaseUnitCount = viewModel.CaseUnitCount,
                SlaveUnitCount = viewModel.SlaveUnitCount,
                TotalUnitCount = viewModel.TotalUnitCount,
                Yield = viewModel.Yield,
                ActualYield = viewModel.ActualYield,
                TargetConversionEfficiency = viewModel.TargetConversionEfficiency,
                ProcentualYieldDifference = viewModel.ProcentualYieldDifference,
                McDPOSRefIDAI = viewModel.McDPOSRefIDAI,
                ItemIDAI = viewModel.ItemIDAI,
                Source = viewModel.Source,
                CaseUPCEANNumber = viewModel.CaseUPCEANNumber
            };
        }
    }
}