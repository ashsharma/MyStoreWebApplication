﻿using System;
using System.Collections.Generic;
using Mcd.MyStore.InventoryService.Common.DataContracts.CountingListEditor;
using Mcd.MyStore.InventoryService.Common.Definitions;

namespace MyStoreTabletAppClient.Data.CountingListEngine
{
    public class CountingListContainerDto
    {
        #region Constructor

        public CountingListContainerDto()
        {
            Warehouses = new List<WarehouseItemDto>();
            Items = new List<CountingListItemDto>();
            CountingLists = new List<CountingList>();
        }

        #endregion

        public CountingListType CountingListType { get; set; }

        public List<WarehouseItemDto> Warehouses { get; set; }
        public List<CountingListItemDto> Items { get; set; }
        public string Name { get; set; }

        public bool IsCompleted;
        public string Witness { get; set; }

        public List<CountingList> CountingLists { get; set; }

        public DateTime? Timestamp { get; set; }
    }
}
