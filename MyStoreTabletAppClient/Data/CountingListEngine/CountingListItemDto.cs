﻿
using System;

namespace MyStoreTabletAppClient.Data.CountingListEngine
{
    public class CountingListItemDto
    {
        public static CountingListItemDto FromCountingListItemCacheDto(CountingListItemCacheDto cacheDto)
        {
            return new CountingListItemDto
            {
                Timestamp = cacheDto.Timestamp,
                ItemID = cacheDto.ItemID,
                UnitsPerCase = cacheDto.UnitsPerCase,
                SlaveUnitsPerCase = cacheDto.SlaveUnitsPerCase,
                SlaveUnitOfMeasureCode = cacheDto.SlaveUnitOfMeasureCode,
                CaseUnitOfMeasureCode = cacheDto.CaseUnitOfMeasureCode,
                UnitOfMeasureDescription = cacheDto.UnitOfMeasureDescription,
                UnitOfMeasureCode = cacheDto.UnitOfMeasureCode,
                PricePerCase = cacheDto.PricePerCase,
                McDProductCode = cacheDto.McDProductCode,
                EnvironmentalCode = cacheDto.EnvironmentalCode,
                McDPOSRefID = cacheDto.McDPOSRefID,
                Description = cacheDto.Description,
                LocationID = cacheDto.LocationID,
                StoragePlaceID = cacheDto.StoragePlaceID,
                LocationShortName = cacheDto.LocationShortName,
                IsCounted = cacheDto.IsCounted,
                ExpectedUnitCount = cacheDto.ExpectedUnitCount,
                UnitCount = cacheDto.UnitCount,
                CaseUnitCount = cacheDto.CaseUnitCount,
                SlaveUnitCount = cacheDto.SlaveUnitCount,
                TotalUnitCount = cacheDto.TotalUnitCount,
                Yield = cacheDto.Yield,
                ActualYield = cacheDto.ActualYield,
                TargetConversionEfficiency = cacheDto.TargetConversionEfficiency,
                ProcentualYieldDifference = cacheDto.ProcentualYieldDifference,
                McDPOSRefIDAI = cacheDto.McDPOSRefIDAI,
                ItemIDAI = cacheDto.ItemIDAI,
                Source = cacheDto.Source,
                CaseUPCEANNumber = cacheDto.CaseUPCEANNumber,
                TargetStockWithoutInventoryCounts = cacheDto.TargetStockWithoutInventoryCounts
            };
        }

        public DateTime Timestamp { get; set; }
        public int ItemID { get; set; }

        #region AS_ShippingUnit Info

        public decimal UnitsPerCase { get; set; }

        public decimal SlaveUnitsPerCase { get; set; }

        public string SlaveUnitOfMeasureCode { get; set; }

        public string CaseUnitOfMeasureCode { get; set; }

        public string UnitOfMeasureDescription { get; set; }

        public string UnitOfMeasureCode { get; set; }

        public decimal PricePerCase { get; set; }

        public string McDProductCode { get; set; }

        public string EnvironmentalCode { get; set; }

        public string ItemComment { get; set; }

        public bool IsItemLocked { get; set; }

        public bool IsItemDelisted { get; set; }

        public bool IsExpired { get; set; }

        public int? RemapToShippingUnitMcdPosRefID { get; set; }

        #endregion

        #region AS_Item Info

        public int McDPOSRefID { get; set; }

        public string Description { get; set; }

        #endregion

        #region CO_Warehouse_Item Info

        /// <summary>
        /// The location unique identifier.
        /// </summary>
        public int? LocationID { get; set; }

        /// <summary>
        /// The sub location unique identifier.
        /// </summary>
        public int? SubLocationID { get; set; }

        /// <summary>
        /// The position in the warehouse.
        /// </summary>
        public int? StoragePlaceID { get; set; }

        #endregion

        #region TR_ItemTransactionLineItem Info

        public bool IsCounted { get; set; }

        public bool IsItemAlreadyCountedForBusinessDay { get; set; }

        /// <summary>
        /// The expected count. 
        /// This is nullable to give the CountingListInputProviders the opportunity to populate this field if they have the data
        /// available. If it is null, it will be calculated by the Counting List Engine
        /// </summary>
        public decimal? ExpectedUnitCount { get; set; }

        public decimal? UnitCount { get; set; }

        public decimal? CaseUnitCount { get; set; }

        public decimal? SlaveUnitCount { get; set; }

        public decimal? TotalUnitCount { get; set; }
        public decimal Yield { get; set; }
        public decimal ActualYield { get; set; }
        public decimal TargetConversionEfficiency { get; set; }
        public decimal ProcentualYieldDifference { get; set; }
        public decimal DifferenceAmount { get; set; }
        public decimal DifferenceValue { get; set; }

        #endregion

        #region AssemblyItem Info

        public int McDPOSRefIDAI { get; set; }

        public int ItemIDAI { get; set; }

        public string DescriptionAI { get; set; }

        #endregion

        /// <summary>
        /// Name of provider (if any)
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        /// WinApp - Determine if still stored on tablet (cache)
        /// </summary>
        public bool IsLocal { get; set; }

        public string CaseUPCEANNumber { get; set; }

        public bool IsGroupHeader { get; set; }
        public decimal MonetaryValue { get; set; }

        public decimal CalculatedExpectedUnitCount { get; set; }


        // only for delisted items available
        // book all counts as waste (create transaction)
        public bool HasBookAsWasteFlag { get; set; }

        public bool IsLocked { get; set; }

        public bool HasRemoveFlag { get; set; }

        public string LocationShortName { get; set; }

        public decimal TargetStockWithoutInventoryCounts { get; set; }
    }
}
