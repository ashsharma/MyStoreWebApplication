﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Mcd.MyStore.InventoryService.Common.DataContracts.CountingListEditor
{
    public class CountingListItem
    {

        [DataMember]
        [XmlIgnore]
        public int ItemID { get; set; }

        [DataMember]
        public int PosID { get; set; }

        [DataMember]
        public string ItemName { get; set; }

        [DataMember]
        public System.Collections.Generic.List<int> AssignedCountingLists { get; set; }

        [DataMember]
        public string Source { get; set; }
    }
}
