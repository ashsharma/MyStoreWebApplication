﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Mcd.MyStore.InventoryService.Common.DataContracts.CountingListEditor
{
    [DataContract]
    public class CountingList
    {
        [DataMember]
        [XmlIgnore]
        public int? CountingListID { get; set; }

        [DataMember]
        public string CountingListName { get; set; }

        [DataMember]
        [XmlIgnore]
        public int? CreatorID { get; set; }

        [DataMember]
        [XmlIgnore]
        public string CreatorName { get; set; }

        [DataMember]
        [XmlIgnore]
        public DateTime CreationDate { get; set; }

        [DataMember]
        [XmlIgnore]
        public DateTime? LastUpdateDate { get; set; }

        [DataMember]
        [XmlIgnore]
        public int? LastUpdateUserID { get; set; } 

        [DataMember]
        [XmlIgnore]
        public string LastUpdateUserName { get; set; }

        [DataMember]
        [XmlIgnore]
        public int? BaseListID { get; set; } 

        [DataMember]
        [XmlIgnore]
        public string BaseListName { get; set; }

        [DataMember]
        public List<CountingListItem> CountingListItems { get; set; }

        [DataMember]
        public List<InputListProviderConfiguration> InputListProviders { get; set; }

        [DataMember]
        public string CountingListPeriod { get; set; }

        [DataMember]
        public int? Weekday { get; set; }

        [DataMember]
        public int? Month { get; set; }

        [DataMember]
        public int? Day { get; set; }

        [DataMember]
        public DateTime? CountingListDate { get; set; }

        [DataMember]
        public bool? CountOnLastDayOfMonth { get; set; }

        public CountingList()
        {
            CountingListItems = new List<CountingListItem>();
        }

    }
}
