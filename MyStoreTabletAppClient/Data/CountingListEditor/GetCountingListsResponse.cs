﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Mcd.MyStore.InventoryService.Common.DataContracts.CountingListEditor
{
    [DataContract]
    public class GetCountingListsResponse
    {
        [DataMember]
        public bool Success { get; set; }

        [DataMember]
        public List<CountingList> CountingLists { get; set; }
        
    }
}
