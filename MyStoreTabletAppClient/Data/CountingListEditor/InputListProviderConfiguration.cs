﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Mcd.MyStore.InventoryService.Common.DataContracts.CountingListEditor
{
    [DataContract]
    public class InputListProviderConfiguration
    {
        [DataMember]
        public bool Enabled { get; set; }

        [DataMember]
        public string ProviderName { get; set; }

        [DataMember]
        public int NumberOfItems { get; set; }

        [DataMember]
        public bool IsReadOnly { get; set; }
    }
}
