﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyStoreTabletAppClient.Data
{
    public class Warehouse
    {
        public int ItemId { get; set; }
        public int ItemNumber { get; set; }
        public string Description { get; set; }
        public decimal CaseUnit { get; set; }
        public decimal SubUnit { get; set; }
        public decimal Unit { get; set; }
        public decimal Total { get; set; }
        public decimal Expected { get; set; }
        public decimal Difference { get; set; }
        public bool Counted { get; set; }
    }
}