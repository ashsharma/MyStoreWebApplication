﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyStoreTabletAppClient.Data.Models
{
    public class Credential
    {
        public string userName { get; set; }
        public string password { get; set; }
    }

}
