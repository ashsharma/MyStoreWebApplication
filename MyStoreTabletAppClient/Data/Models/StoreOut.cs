﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyStoreTabletAppClient.Data.Models
{
    public class StoreOut
    {
        [Key]
        public int Id { get; set; }
        public int StoreNumber { get; set; }
        public string StoreName { get; set; }
        public string StoreAddress { get; set; }
        public string StoreIp { get; set; }
        public int? CountryId { get; set; }
        public int DistributionRegionId { get; set; }
    }
}
